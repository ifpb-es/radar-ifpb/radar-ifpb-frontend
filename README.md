# Frontend - Radar IF

Projeto da disciplina de Engenharia de Software do Mestrado em TI - PPGTI 2020 do IFPB - Campus João Pessoa.

Esse repositório constitui o **Frontend** do sistema, sendo construído utilizando-se o _framework_ Angular, em sua versão 7.

## Preparação

Primeiramente, devem ser instalados o `node` e o `npm` para execução do projeto. Para isso, adicione o repositório do 
_NodeServer_ ao `yum` com:

```bash
$ curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
```

Com isso, instale o `node` com :

```bash
$ sudo yum install nodejs
```

Após, verifique a instalação do `node` com `node --version` e a instalação do `npm` com `npm --version`. As saídas devem ser `v10.x.y` e `6.x.y`, respectivamente.

#### Dependências

Para **instalação das dependências**, execute o comando `npm i` estando na raiz do projeto. Deverá ser criada a pasta `node_modules`.

#### Execução

Para colocar o projeto em **execução**, execute o comando `npm start`. Com isso, abra o seu _browser_ e acesse `http://localhost:4200`.

