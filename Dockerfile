FROM centos:centos7
LABEL maintainer="Pedro Vitor Cerqueira Pacheco <pedrovcpacheco@yahoo.com>"
WORKDIR /opt
RUN yum clean all &&  yum  install -y \
    epel-release  \
    git  \
    sudo \
    curl \
    && curl -sL https://rpm.nodesource.com/setup_9.x | sudo -E bash -  \
    && yum  install -y nodejs  \
    && rm -rf /var/cache/yum   \
    && git clone https://gitlab.com/ifpb-es/radar-ifpb/radar-ifpb-frontend.git
WORKDIR /opt/radar-ifpb-frontend
RUN npm install -g @angular/cli@^7.2.2   && npm install 
ENTRYPOINT ["ng","serve"]
CMD ["--host","0.0.0.0","--port", "4200", "--disableHostCheck", "true"]
EXPOSE 4200




