import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() backGroundColor: string;
  @Input() fontColor: string;
  @Input() icon: string;
  @Input() texto: string;
  constructor() { }

  ngOnInit() {
  }

}
