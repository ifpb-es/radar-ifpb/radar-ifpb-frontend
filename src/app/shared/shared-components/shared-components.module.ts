import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContainerComponent } from './container/container.component';
import { HeaderComponent } from './header/header.component';
import { CardComponent } from './card/card.component';
import { LoaderComponent } from './loader/loader.component';
import { LoaderService } from '../shared-services/loader.service';
import { SobreComponentDialog } from './sobre/sobre.component';

@NgModule({
  imports: [
    CommonModule,
    AngularMaterialModule,
    FlexLayoutModule
  ],
  declarations: [
    HeaderComponent,
    ContainerComponent,
    CardComponent,
    LoaderComponent,
    SobreComponentDialog
  ],
  exports: [
    HeaderComponent,
    ContainerComponent,
    CardComponent,
    LoaderComponent
  ],
  providers: [
    LoaderService
  ],
  entryComponents: [
    SobreComponentDialog
  ]
})
export class SharedComponentsModule { }
