export class Campus {
    constructor(public campus: string) { }
    static getTodos(): Campus[] {
        return [
            new Campus('Todos'),
            new Campus('CAMPUS PICUÍ'),
            new Campus('CAMPUS CAMPINA GRANDE'),
            new Campus('CAMPUS ITABAIANA'),
            new Campus('CAMPUS SANTA RITA'),
            new Campus('CAMPUS MONTEIRO'),
            new Campus('CAMPUS CATOLÉ DO ROCHA'),
            new Campus('REITORIA'),
            new Campus('CAMPUS PRINCESA ISABEL'),
            new Campus('CAMPUS ITAPORANGA'),
            new Campus('CAMPUS SOUSA'),
            new Campus('CAMPUS PATOS'),
            new Campus('CAMPUS CAJAZEIRAS'),
            new Campus('CAMPUS GUARABIRA'),
            new Campus('CAMPUS JOÃO PESSOA'),
            new Campus('CAMPUS CABEDELO'),
            new Campus('CAMPUS ESPERANÇA')
        ];
    }
}
