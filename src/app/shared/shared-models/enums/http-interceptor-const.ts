export enum InterceptorConst {
  MSG_ERROR_SERVIDOR_TITULO = 'Foi detectado uma anomalia interna no servidor',
  MSG_ERROR_SERVIDOR = 'Ocorreu um problema interno no servidor. O sistema detectou uma anomalia' +
                        'e não é possível processar sua requisição. Procure o administrador do sistema.'
}
