import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, MatSnackBarConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackService {

  matSnackBarRef: MatSnackBarRef<any>;
  botaoFechar = 'Fechar';

  constructor(
    private snackBar: MatSnackBar
  ) { }
  // warnning
  alertar(mensagem: string, config?: MatSnackBarConfig) {
    this.matSnackBarRef = this.snackBar.open(mensagem, this.botaoFechar, {
      ...config,
      duration: 4000,
    });
  }
  // info
  informar(mensagem: string, config?: MatSnackBarConfig) {
    this.matSnackBarRef = this.snackBar.open(mensagem, this.botaoFechar, {
      ...config,
      duration: 4000,
    });
    return this.matSnackBarRef;
  }
  // danger
  advertir(mensagem: string, config?: MatSnackBarConfig) {
    this.matSnackBarRef = this.snackBar.open(mensagem, this.botaoFechar, {
      ...config,
      duration: 4000,
      panelClass: 'danger-snack1'
    });
  }

}
