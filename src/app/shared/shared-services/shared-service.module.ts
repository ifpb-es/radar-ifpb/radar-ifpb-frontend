import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnackService } from './snack.service';
import { LoaderService } from './loader.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    SnackService,
    LoaderService
  ],
  entryComponents: [
  ]
})
export class SharedServiceModule { }
