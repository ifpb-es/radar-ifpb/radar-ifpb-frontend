import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  protected urlBase = environment.apiUrl;
  constructor(protected httpClient: HttpClient) { }
}
