import { Validators, AbstractControl } from '@angular/forms';

/**
 * classe de validação custom, onde irá centralizar todas a validações custom
 * @param control - o control do campo
 */
export class CustomValidators extends Validators {

  /**
   * função que valida se o campo de entrada é número
   * @param control - o control do campo
   */
  static onlyNumberLetters(control: AbstractControl) {
    if (control.value && !control.value.match(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/)) {
      return { invalid: true };
    }
    return null;
  }


}
