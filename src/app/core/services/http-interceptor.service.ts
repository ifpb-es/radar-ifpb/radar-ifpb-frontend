
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { InterceptorConst } from 'src/app/shared/shared-models/enums/http-interceptor-const';
import { SnackService } from 'src/app/shared/shared-services/snack.service';
import { Injectable } from '@angular/core';
import { HttpStatus } from 'src/app/shared/shared-models/enums/http-status';


@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(
        private snackService: SnackService
    ) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        const clone = request.clone({
            headers: request.headers.set('key', 'DCtbqRXC8L')
        });

        return next.handle(clone).pipe(
            map((event) => {
                return event;
            }),
            catchError((error) => {
                return this.errorHandler(error);
            })
        );
    }

    private errorHandler(error: any) {
        if (error instanceof HttpErrorResponse) {
            switch (error.status) {
                case HttpStatus.INTERNAL_SERVER_ERROR:
                case HttpStatus.BAD_GATEWAY:
                case HttpStatus.GATEWAY_TIMEOUT:
                case HttpStatus.FORBIDDEN:
                    this.snackService.alertar(InterceptorConst.MSG_ERROR_SERVIDOR_TITULO);
                    break;
            }
        }
        return throwError(error);
    }

}
