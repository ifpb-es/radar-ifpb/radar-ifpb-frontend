import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/shared/shared-services/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const PATHS = {
  info: '/info'
};

@Injectable({
  providedIn: 'root'
})
export class InfoService extends BaseService {

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  getVersaoBackend(): Observable<AppInfo> {
    return this.httpClient.get<AppInfo>(`${this.urlBase}${PATHS.info}`);
  }

  getVersaoFrontEnd(): AppInfo {
    return {version: environment.versao};
  }
}

type AppInfo = { version: string };
