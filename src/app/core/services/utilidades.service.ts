import { Injectable } from '@angular/core';
import * as moment from 'moment';
/**
 * Todas as funções desse serviço são possíveis candidatos a se tornarem Pipes futuramante
 */
@Injectable()
export class UtilidadesService {

  /**
   * verifica se o objeto passado é vazio
   * @param obj 
   */
  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  async compareToList(lista: any[], propName: any) {
    const ordenado = lista.sort((n1, n2) => {
      if (n1[propName] > n2[propName]) {
        return 1;
      }
      if (n1[propName] < n2[propName]) {
        return -1;
      }
      return 0;
    });
    return ordenado;
  }



}
