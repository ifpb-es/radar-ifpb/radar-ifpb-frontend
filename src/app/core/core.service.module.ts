import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { HttpUtilsService } from './services/http-utils.service';
import { UtilidadesService } from './services/utilidades.service';
import { LoaderInterceptor } from './services/loader.interceptor';
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    HttpUtilsService,
    UtilidadesService,
    HttpInterceptorService,
    LoaderInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    { provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    }
  ],
  entryComponents: [],
  exports: [
  ]
})
export class CoreModule { }
