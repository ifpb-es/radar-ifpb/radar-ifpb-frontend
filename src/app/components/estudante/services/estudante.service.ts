import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/shared/shared-services/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProgramaEstudantil } from '../models/programas.model';
import { PesquisaCampus } from '../../shared/models/pesquisa.campus.model';

const PATHS = {
  estudantis: '/atividades/estudantis',
  programas: '/programas',
  bolsas: '/bolsas',
  campus: '/campus',
  valores: '/valores'
};

@Injectable()
export class EstudantesService extends BaseService {

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  consultarTotalProgramas(): Observable<ProgramaEstudantil[]> {
    return this.httpClient.get<ProgramaEstudantil[]>(`${this.urlBase}${PATHS.estudantis}${PATHS.programas}`);
  }


  consultarTotalProgramasPorCampus(): Observable<PesquisaCampus[]> {
    return this.httpClient.get<PesquisaCampus[]>(`${this.urlBase}${PATHS.estudantis}${PATHS.programas}${PATHS.campus}`);
  }

  consultarTotalValoresPorBolsa(): Observable<ProgramaEstudantil[]> {
    return this.httpClient.get<ProgramaEstudantil[]>(`${this.urlBase}${PATHS.estudantis}${PATHS.bolsas}${PATHS.valores}`);
  }

}
