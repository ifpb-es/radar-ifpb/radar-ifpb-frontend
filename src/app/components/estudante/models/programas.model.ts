export interface ProgramaEstudantil {
    categoria: string;
    total: number;
    atendimentos: Map<string, number>;
    valorTotal?: number;
}
