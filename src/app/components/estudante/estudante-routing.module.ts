import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstudanteComponent } from './components/estudante.component';

const routes: Routes = [{
  path: '',
  component: EstudanteComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstudanteRoutingModule { }
