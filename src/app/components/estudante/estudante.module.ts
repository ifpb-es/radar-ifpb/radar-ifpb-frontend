import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { SharedComponentsModule } from 'src/app/shared/shared-components/shared-components.module';
import { EstudanteComponent } from './components/estudante.component';
import { EstudanteRoutingModule } from './estudante-routing.module';
import { NumeroProgramasComponent } from './components/numero-programas/numero-programas.component';
import { EstudantesService } from './services/estudante.service';
import { ChartsModule } from 'ng2-charts';
import { SharedComponentsHomeModule } from '../shared/components/shared.component.module';
import { ValoresProgramasComponent } from './components/valores-programas/valores-programas.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DetalheNumeroProgramasComponent } from './components/numero-programas/detalhe-numero-programas/detalhe-numero-programas.component';

@NgModule({
  declarations: [
    EstudanteComponent,
    NumeroProgramasComponent,
    ValoresProgramasComponent,
    DetalheNumeroProgramasComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    SharedComponentsModule,
    EstudanteRoutingModule,
    ChartsModule,
    SharedComponentsHomeModule,
    FlexLayoutModule
  ],
  providers: [
    EstudantesService
  ],
  entryComponents:[
    DetalheNumeroProgramasComponent
  ]
})
export class EstudanteModule { }
