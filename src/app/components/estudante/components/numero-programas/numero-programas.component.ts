import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ProgramaEstudantil } from '../../models/programas.model';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { DetalheNumeroProgramasComponent } from './detalhe-numero-programas/detalhe-numero-programas.component';

@Component({
  selector: 'app-numero-programas',
  templateUrl: './numero-programas.component.html',
  styleUrls: ['./numero-programas.component.css']
})
export class NumeroProgramasComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['categoria', 'total', 'acao'];
  dataSource = new MatTableDataSource<ProgramaEstudantil>();

  // tslint:disable-next-line: no-input-rename
  @Input('resultado') numeroProgramas: ProgramaEstudantil[];

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<ProgramaEstudantil>(this.numeroProgramas);
    this.dataSource.paginator = this.paginator;
    this.translateMatPaginator(this.dataSource.paginator);
  }

  translateMatPaginator(paginator: MatPaginator) {
    paginator._intl.firstPageLabel = 'Primeira Página';
    paginator._intl.itemsPerPageLabel = 'Exibir';
    paginator._intl.lastPageLabel = 'Última Página';
    paginator._intl.nextPageLabel = 'Próxima Página';
    paginator._intl.previousPageLabel = 'Página Anterior';
  }

  abrirDetalhe(item: ProgramaEstudantil) {
    this.dialog.open(DetalheNumeroProgramasComponent, {
      width: '600px',
      data: {
        tipoBolsa: item.categoria,
        atendimentos: item.atendimentos
      }
    });
  }

}
