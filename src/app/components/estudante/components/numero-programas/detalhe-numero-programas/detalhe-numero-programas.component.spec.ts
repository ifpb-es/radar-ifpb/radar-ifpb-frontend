import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalheNumeroProgramasComponent } from './detalhe-numero-programas.component';

describe('DetalheNumeroProgramasComponent', () => {
  let component: DetalheNumeroProgramasComponent;
  let fixture: ComponentFixture<DetalheNumeroProgramasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalheNumeroProgramasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalheNumeroProgramasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
