import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-detalhe-numero-programas',
  templateUrl: './detalhe-numero-programas.component.html',
  styleUrls: ['./detalhe-numero-programas.component.css']
})
export class DetalheNumeroProgramasComponent implements OnInit {

  tipoBolsa: string;
  campus: Map<string, number>;
  constructor(@Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit() {
    this.setDados();
  }

  private setDados() {
    this.tipoBolsa = this.data.tipoBolsa;
    this.campus = new Map();

    Object.keys(this.data.atendimentos).forEach(key => {
      this.campus.set(key, this.data.atendimentos[key]);
    });
  }

}
