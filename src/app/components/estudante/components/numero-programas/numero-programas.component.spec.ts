import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumeroProgramasComponent } from './numero-programas.component';

describe('NumeroProgramasComponent', () => {
  let component: NumeroProgramasComponent;
  let fixture: ComponentFixture<NumeroProgramasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumeroProgramasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumeroProgramasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
