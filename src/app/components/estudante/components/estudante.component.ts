import { Component, OnInit } from '@angular/core';
import { EstudantesService } from '../services/estudante.service';
import { ProgramaEstudantil } from '../models/programas.model';
import { SnackService } from 'src/app/shared/shared-services/snack.service';
import { forkJoin } from 'rxjs';
import { UtilidadesService } from 'src/app/core/services/utilidades.service';
import { PesquisaCampus } from '../../shared/models/pesquisa.campus.model';

@Component({
  selector: 'app-estudante',
  templateUrl: './estudante.component.html',
  styleUrls: ['./estudante.component.css']
})
export class EstudanteComponent implements OnInit {

  programasEstudantis: ProgramaEstudantil[];
  numeroProgramasPorCampus: PesquisaCampus[];
  programasValores: ProgramaEstudantil[];

  constructor(private estudanteService: EstudantesService,
    private snackService: SnackService,
    private utilizadeService: UtilidadesService
  ) { }

  ngOnInit() {
    forkJoin(
      this.estudanteService.consultarTotalProgramas(),
      this.estudanteService.consultarTotalProgramasPorCampus(),
      this.estudanteService.consultarTotalValoresPorBolsa()
    ).subscribe(resultado => {

      this.setNumeroProgramas(resultado[0]);
      this.setNumeroProjetoPorCampus(resultado[1]);
      this.setValoresProgramas(resultado[2]);

    }, () => {
      this.snackService.alertar('Alguma coisa não saiu como esperado, por favor tente novamente mais tarde!');
    });
  }

  async setNumeroProgramas(response: ProgramaEstudantil[]) {
    if (Array.isArray(response)) {
      response = await this.utilizadeService.compareToList(response, 'total');
      response.reverse();
      this.programasEstudantis = response;
    } else {
      this.tratarRetorno();
    }
  }

  async setValoresProgramas(response: ProgramaEstudantil[]) {
    if (Array.isArray(response)) {
      response = await this.utilizadeService.compareToList(response, 'valorTotal');
      response.reverse();
      this.programasValores = response.slice(0, 4);
    } else {
      this.tratarRetorno();
    }
  }

  private setNumeroProjetoPorCampus(resultado: PesquisaCampus[]) {
    if (Array.isArray(resultado)) {
      this.numeroProgramasPorCampus = resultado;
    } else {
      this.tratarRetorno();
    }
  }

  private tratarRetorno(): void {
    this.snackService.informar('Nenhum resultado encontrado.');
  }

}
