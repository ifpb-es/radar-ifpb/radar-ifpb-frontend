import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValoresProgramasComponent } from './valores-programas.component';

describe('ValoresProgramasComponent', () => {
  let component: ValoresProgramasComponent;
  let fixture: ComponentFixture<ValoresProgramasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValoresProgramasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValoresProgramasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
