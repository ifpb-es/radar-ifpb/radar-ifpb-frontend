import { Component, OnInit, Input  } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartType, ChartOptions } from 'chart.js';
import { ProgramaEstudantil } from '../../models/programas.model';

@Component({
  selector: 'app-valores-programas',
  templateUrl: './valores-programas.component.html',
  styleUrls: ['./valores-programas.component.css']
})
export class ValoresProgramasComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('resultado') dataSet: ProgramaEstudantil[];

  public doughnutChartLabels: Label[] = [];
  public doughnutChartData: number[] = [];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top'
    },
    tooltips: {
      callbacks: {
        label: (item, data) => {
          const formatter = new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
          });
          return data.labels[item.index] + ': ' +
          formatter.format(Number(data.datasets[0].data[item.index]));
        }
      }
    }
  };
  constructor() { }

  ngOnInit() {
    this.initChart();
  }

  private initChart(): void {
    this.dataSet.forEach((item: ProgramaEstudantil) => {
      this.doughnutChartLabels.push(item.categoria);
      this.doughnutChartData.push(item.valorTotal);
    });
  }

}

