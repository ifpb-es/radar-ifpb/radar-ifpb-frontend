import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { SharedComponentsModule } from 'src/app/shared/shared-components/shared-components.module';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CoreModule } from 'src/app/core/core.service.module';

@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent
  ],
  imports: [
    HomeRoutingModule,
    FlexLayoutModule,
    AngularMaterialModule,
    SharedComponentsModule,
    CoreModule,
    CommonModule
  ]
})
export class HomeModule { }
