import { Component, OnInit } from '@angular/core';
import { InfoService } from 'src/app/core/services/info.service';
import { MatDialog } from '@angular/material';
import { SobreComponentDialog } from 'src/app/shared/shared-components/sobre/sobre.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  versao = {
    frontend: 'v.0.0.0',
    backend: 'v.0.0.0'
  };

  constructor(private infoServiceService: InfoService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getVersaoFrontEnd();
    this.getVersaoBackend();
  }

  private getVersaoFrontEnd() {
    this.versao.frontend = 'v.' + this.infoServiceService.getVersaoFrontEnd().version;
  }

  private getVersaoBackend() {
    this.infoServiceService.getVersaoBackend().subscribe(versao => {
      this.versao.backend = versao.version;
    // tslint:disable-next-line: no-shadowed-variable
    });
  }

  abrirSobre() {
    this.dialog.open(SobreComponentDialog, {
      width: '600px'
    });
  }
}
