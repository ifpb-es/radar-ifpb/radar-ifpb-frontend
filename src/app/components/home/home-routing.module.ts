import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { DashboardComponent } from '../dashboard/dashboard.component';

const routes: Routes = [{
  path: 'home',
  component: HomeComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      component: DashboardComponent
    },
    {
      path: 'extensao',
      loadChildren: '../extensao/extensao.module#ExtensaoModule'
    },
    {
      path: 'pesquisa',
      loadChildren: '../pesquisa/pesquisa.module#PesquisaModule'
    },
    {
      path: 'estudante',
      loadChildren: '../estudante/estudante.module#EstudanteModule'
    },
    {
      path: 'administracao',
      loadChildren: '../administracao/administracao.module#AdministracaoModule'
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
