import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { SharedComponentsModule } from 'src/app/shared/shared-components/shared-components.module';
import { AdministracaoComponent } from './components/administracao.component';
import { AdministracaoRoutingModule } from './administracao-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsModule } from 'ng2-charts';
import { PatrimoniosPorSituacoesComponent } from './components/patrimonios-por-situacoes/patrimonios-por-situacoes.component';
import { AdministracaoService } from './services/administracao.service';
import { PatrimonioPorEstadoComponent } from './components/patrimonio-por-estado/patrimonio-por-estado.component';
import { PatrimoniosPorValoresComponent } from './components/patrimonios-por-valores/patrimonios-por-valores.component';

@NgModule({
  declarations: [AdministracaoComponent, PatrimoniosPorSituacoesComponent, PatrimonioPorEstadoComponent, PatrimoniosPorValoresComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    SharedComponentsModule,
    AdministracaoRoutingModule,
    FlexLayoutModule,
    ChartsModule
  ],
  providers: [
    AdministracaoService
  ]
})
export class AdministracaoModule { }
