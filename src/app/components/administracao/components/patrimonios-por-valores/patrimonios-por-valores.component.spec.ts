import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrimoniosPorValoresComponent } from './patrimonios-por-valores.component';

describe('PatrimoniosPorValoresComponent', () => {
  let component: PatrimoniosPorValoresComponent;
  let fixture: ComponentFixture<PatrimoniosPorValoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatrimoniosPorValoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatrimoniosPorValoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
