import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { ValorPatrinomioCampus } from '../../models/valor-patrimonio-campus.model';
@Component({
  selector: 'app-patrimonios-por-valores',
  templateUrl: './patrimonios-por-valores.component.html',
  styleUrls: ['./patrimonios-por-valores.component.css']
})
export class PatrimoniosPorValoresComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('resultado') resultado: ValorPatrinomioCampus[];

  public barChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    },
    tooltips: {
      callbacks: {
        label: (item) => {
          const formatter = new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
          });
          return formatter.format(Number(item.value));
        }
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [
    { data: [], label: 'Inicial' },
    { data: [], label: 'Líquido' }
  ];

  constructor() { }

  ngOnInit() {
    this.setDados();
  }

  private setDados() {
    this.resultado.forEach((r: ValorPatrinomioCampus) => {
      this.barChartData[0].data.push(r.inicial);
      this.barChartData[1].data.push(r.liquido);
      this.barChartLabels.push(r.campus);
    });
  }
}
