import { Component, OnInit, Input } from '@angular/core';
import { EstadoPatrinomio } from '../../models/estado-patrimonio.model';
import { ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-patrimonio-por-estado',
  templateUrl: './patrimonio-por-estado.component.html',
  styleUrls: ['./patrimonio-por-estado.component.css']
})
export class PatrimonioPorEstadoComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('resultado') resultado: EstadoPatrinomio[];

  public doughnutChartLabels: Label[] = [];
  public doughnutChartData: number[] = [];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };

  constructor() { }

  ngOnInit() {
    this.resultado.forEach((r: EstadoPatrinomio) => {
      this.doughnutChartLabels.push(r.estado);
      this.doughnutChartData.push(r.total);
    });
  }
}
