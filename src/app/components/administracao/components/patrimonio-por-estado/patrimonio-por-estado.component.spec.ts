import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrimonioPorEstadoComponent } from './patrimonio-por-estado.component';

describe('PatrimonioPorEstadoComponent', () => {
  let component: PatrimonioPorEstadoComponent;
  let fixture: ComponentFixture<PatrimonioPorEstadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatrimonioPorEstadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatrimonioPorEstadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
