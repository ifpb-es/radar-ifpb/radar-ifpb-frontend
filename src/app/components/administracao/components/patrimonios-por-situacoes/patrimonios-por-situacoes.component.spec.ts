import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatrimoniosPorSituacoesComponent } from './patrimonios-por-situacoes.component';

describe('PatrimoniosPorSituacoesComponent', () => {
  let component: PatrimoniosPorSituacoesComponent;
  let fixture: ComponentFixture<PatrimoniosPorSituacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatrimoniosPorSituacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatrimoniosPorSituacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
