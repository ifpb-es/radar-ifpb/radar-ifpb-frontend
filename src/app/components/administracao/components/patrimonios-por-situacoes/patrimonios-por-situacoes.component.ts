import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { SituacaoPatrinomio } from '../../models/sitacao-patrimonio.model';

@Component({
  selector: 'app-patrimonios-por-situacoes',
  templateUrl: './patrimonios-por-situacoes.component.html',
  styleUrls: ['./patrimonios-por-situacoes.component.css']
})
export class PatrimoniosPorSituacoesComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('resultado') resultado: SituacaoPatrinomio[];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [ ];

  constructor() { }

  ngOnInit() {
    this.resultado.forEach((r: SituacaoPatrinomio) => {
      this.pieChartLabels.push(r.situacao);
      this.pieChartData.push(r.total);
    });
  }
}
