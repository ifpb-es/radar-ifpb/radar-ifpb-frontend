import { Component, OnInit } from '@angular/core';
import { AdministracaoService } from '../services/administracao.service';
import { SituacaoPatrinomio } from '../models/sitacao-patrimonio.model';
import { SnackService } from 'src/app/shared/shared-services/snack.service';
import { UtilidadesService } from 'src/app/core/services/utilidades.service';
import { forkJoin } from 'rxjs';
import { EstadoPatrinomio } from '../models/estado-patrimonio.model';
import { ValorPatrinomioCampus } from '../models/valor-patrimonio-campus.model';

@Component({
  selector: 'app-administracao',
  templateUrl: './administracao.component.html',
  styleUrls: ['./administracao.component.css']
})
export class AdministracaoComponent implements OnInit {

  situacoesPatrimonios: SituacaoPatrinomio[];
  estadosConservacaoPatrimonios: EstadoPatrinomio[];
  patrimoniosPorValores: ValorPatrinomioCampus[] = new Array<ValorPatrinomioCampus>();

  constructor(private administracaoService: AdministracaoService,
    private snackService: SnackService,
    private utilizadeService: UtilidadesService) { }

  ngOnInit() {
    forkJoin(
      this.administracaoService.consultarPatrimoniosPorSituacao(),
      this.administracaoService.consultarPatrimoniosPorEstadoConservacao(),
      this.administracaoService.consultarPatrimoniosValoresIniciais(),
      this.administracaoService.consultarPatrimoniosValoresLiquido()
    ).subscribe(resultado => {
      this.setPatrimonioPorSituacao(resultado[0]);
      this.setPatrimonioPorEstado(resultado[1]);
      this.setPatrimoniosValoresIniciais(resultado[2], resultado[3]);
    }, () => {
      this.snackService.alertar('Alguma coisa não saiu como esperado, por favor tente novamente mais tarde!');
    });
  }

  async setPatrimoniosValoresIniciais(resultadoIniciais, resultadoLiquidos: ValorPatrinomioCampus[]) {
    if (Array.isArray(resultadoIniciais)) {
      resultadoIniciais.forEach((r: ValorPatrinomioCampus) => {
        if (r.campus !== null ) {
          this.patrimoniosPorValores.push({ campus: r.campus, inicial: r.total });
        }
      });

      resultadoLiquidos.forEach((r: ValorPatrinomioCampus) => {
        const index = this.patrimoniosPorValores.findIndex(p => p.campus === r.campus);
        if (index !== -1) {
          this.patrimoniosPorValores[index].liquido = r.total;
        }
      });

      this.patrimoniosPorValores = await this.utilizadeService.compareToList(this.patrimoniosPorValores, 'campus');
    } else {
      this.tratarRetorno();
    }
  }

  private setPatrimonioPorEstado(resultado: EstadoPatrinomio[]) {
    if (Array.isArray(resultado)) {
      this.estadosConservacaoPatrimonios = resultado;
    } else {
      this.tratarRetorno();
    }
  }

  private setPatrimonioPorSituacao(resultado: SituacaoPatrinomio[]) {
    if (Array.isArray(resultado)) {
      this.situacoesPatrimonios = resultado;
    } else {
      this.tratarRetorno();
    }
  }

  private tratarRetorno(): void {
    this.snackService.informar('Nenhum resultado encontrado.');
  }

}
