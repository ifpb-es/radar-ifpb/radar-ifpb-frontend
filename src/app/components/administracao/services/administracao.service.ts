import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/shared/shared-services/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SituacaoPatrinomio } from '../models/sitacao-patrimonio.model';
import { EstadoPatrinomio } from '../models/estado-patrimonio.model';
import { ValorPatrinomioCampus } from '../models/valor-patrimonio-campus.model';

const PATHS = {
  administracao: '/administracao',
  patrimonios: '/patrimonios',
  situacoes: '/situacoes',
  estadoConservacao: '/estado-conservacao',
  valores: '/valores',
  inicial: '/inicial',
  liquido: '/liquido'
};

@Injectable()
export class AdministracaoService extends BaseService {

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  consultarPatrimoniosPorSituacao(nomeCampus?: string): Observable<SituacaoPatrinomio[]> {
    return this.httpClient.get<SituacaoPatrinomio[]>(`${this.urlBase}${PATHS.administracao}${PATHS.patrimonios}${PATHS.situacoes}`,
      { params: this.setParam(nomeCampus) });
  }

  consultarPatrimoniosPorEstadoConservacao(nomeCampus?: string): Observable<EstadoPatrinomio[]> {
    return this.httpClient.get<EstadoPatrinomio[]>(`${this.urlBase}${PATHS.administracao}${PATHS.patrimonios}${PATHS.estadoConservacao}`,
      { params: this.setParam(nomeCampus) });
  }

  consultarPatrimoniosValoresIniciais(): Observable<ValorPatrinomioCampus[]> {
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<ValorPatrinomioCampus[]>(`${this.urlBase}${PATHS.administracao}${PATHS.patrimonios}${PATHS.valores}${PATHS.inicial}`);
  }

  consultarPatrimoniosValoresLiquido(): Observable<ValorPatrinomioCampus[]> {
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<ValorPatrinomioCampus[]>(`${this.urlBase}${PATHS.administracao}${PATHS.patrimonios}${PATHS.valores}${PATHS.liquido}`);
  }

  private setParam(nomeCampus?: string): HttpParams {
    let param = new HttpParams();
    if (nomeCampus) {
      param = param.append('campus', nomeCampus);
    }
    return param;
  }

}
