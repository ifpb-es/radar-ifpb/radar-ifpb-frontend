export interface ValorPatrinomioCampus {
    campus: string;
    total?: number;
    inicial?: number;
    liquido?: number;
}
