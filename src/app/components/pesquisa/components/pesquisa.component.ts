import { Component, OnInit } from '@angular/core';
import { PesquisaService } from '../services/pesquisa.service';
import { UtilidadesService } from 'src/app/core/services/utilidades.service';
import { forkJoin } from 'rxjs';
import { PesquisaCampus } from '../../shared/models/pesquisa.campus.model';
import { PesquisaAreaCampus } from '../../shared/models/pesquisa.area.model';
import { PesquisaAno } from '../../shared/models/pesquisa.ano.model';
import { SnackService } from 'src/app/shared/shared-services/snack.service';
@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.component.html',
  styleUrls: ['./pesquisa.component.css']
})
export class PesquisaComponent implements OnInit {
  numeroProjetoPorCampus: PesquisaCampus[];
  projetosPorAreaConhecimento: PesquisaAreaCampus[];
  projetosPorAno: PesquisaAno[];

  constructor(
    private pesquisaProjetoService: PesquisaService,
    private utilizadeService: UtilidadesService,
    private snackService: SnackService
  ) { }

  ngOnInit() {
    forkJoin(
      this.pesquisaProjetoService.consultarTotalProjetoPorCampus(),
      this.pesquisaProjetoService.consultarTotalProjetoPorAreaConhecimento(),
      this.pesquisaProjetoService.consultarTotalProjetoPorAno()
    ).subscribe(resultado => {

      this.setNumeroProjetoPorCampus(resultado[0]);
      this.setProjetosPorAreaConhecimento(resultado[1]);
      this.setProjetosPorAno(resultado[2]);

    }, () => {
      this.snackService.alertar('Alguma coisa não saiu como esperado, por favor tente novamente mais tarde!');
    });
  }

  consultarNumeroProjetoPorArea(campus?: string) {
    this.pesquisaProjetoService.consultarTotalProjetoPorAreaConhecimento(campus).subscribe((resultado: PesquisaAreaCampus[]) => {
      if (Array.isArray(resultado)) {
        this.setProjetosPorAreaConhecimento(resultado);
      } else {
        this.tratarRetorno();
      }
    }, error => {
      console.error(error);
      this.tratarErro();
    });
  }

  async setProjetosPorAreaConhecimento(resultado: PesquisaAreaCampus[]) {
    resultado = await this.utilizadeService.compareToList(resultado, 'total');
    resultado.reverse();
    this.projetosPorAreaConhecimento = resultado;
  }

  consultarProjetoPorAno(campus?: string) {
    this.pesquisaProjetoService.consultarTotalProjetoPorAno(campus).subscribe((resultado: PesquisaAno[]) => {
      if (Array.isArray(resultado)) {
        this.setProjetosPorAno(resultado);
      } else {
        this.tratarRetorno();
      }
    }, error => {
      console.error(error);
      this.tratarErro();
    });
  }

  async setProjetosPorAno(resultado: PesquisaAno[]) {
    resultado = await this.utilizadeService.compareToList(resultado, 'ano');
    this.projetosPorAno = resultado;
  }

  private setNumeroProjetoPorCampus(resultado: PesquisaCampus[]) {
    if (Array.isArray(resultado)) {
      this.numeroProjetoPorCampus = resultado;
    } else {
      this.tratarRetorno();
    }
  }

  private tratarRetorno(): void {
    this.snackService.informar('Nenhum resultado encontrado.');
  }

  private tratarErro(): void {
    this.snackService.alertar('Não foi possível realizar ação. Tente novamente mais tarde.');
  }

}
