import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { SharedComponentsModule } from 'src/app/shared/shared-components/shared-components.module';
import { PesquisaRoutingModule } from './pesquisa-routing.module';
import { PesquisaComponent } from './components/pesquisa.component';
import { PesquisaService } from './services/pesquisa.service';
import { SharedComponentsHomeModule } from '../shared/components/shared.component.module';
import { SharedServicesHomeModule } from '../shared/services/shared.component.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    PesquisaComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AngularMaterialModule,
    SharedComponentsModule,
    PesquisaRoutingModule,
    SharedComponentsHomeModule,
    SharedServicesHomeModule,
  ],
  providers: [
    PesquisaService
  ]
})
export class PesquisaModule { }
