import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router) {

  }

  ngOnInit() {
  }

  direcionarExtensao(){
    this.router.navigateByUrl('/home/extensao');
  }

  direcionarPesquisa(){
    this.router.navigateByUrl('/home/pesquisa');
  }
  direcionarEstutantis(){
    this.router.navigateByUrl('/home/estudante');
  }
  direcionarAdministracao(){
    this.router.navigateByUrl('/home/administracao');
  }
}
