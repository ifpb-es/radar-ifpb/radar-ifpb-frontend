import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { ChartsModule } from 'ng2-charts';
import { NumeroProjetoCampusComponent } from './numero-projeto-campus/numero-projeto-campus.component';
import { ProjetosAreaConhecimentoComponent } from './projetos-area-conhecimento/projetos-area-conhecimento.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjetosPorAnoComponent } from './projetos-por-ano/projetos-por-ano.component';

@NgModule({
    declarations: [
        NumeroProjetoCampusComponent,
        ProjetosAreaConhecimentoComponent,
        ProjetosPorAnoComponent
    ],
    imports: [
        CommonModule,
        AngularMaterialModule,
        ReactiveFormsModule,
        ChartsModule
    ],
    exports: [
        NumeroProjetoCampusComponent,
        ProjetosAreaConhecimentoComponent,
        ProjetosPorAnoComponent
    ]
})
export class SharedComponentsHomeModule { }
