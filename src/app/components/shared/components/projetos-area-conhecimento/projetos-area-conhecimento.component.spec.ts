import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetosAreaConhecimentoComponent } from './projetos-area-conhecimento.component';

describe('ProjetosAreaConhecimentoComponent', () => {
  let component: ProjetosAreaConhecimentoComponent;
  let fixture: ComponentFixture<ProjetosAreaConhecimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjetosAreaConhecimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetosAreaConhecimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
