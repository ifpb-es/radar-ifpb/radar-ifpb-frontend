import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { Campus } from 'src/app/shared/shared-models/models/campus.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { UtilChartService } from '../../services/util.chart.service';
import { SnackService } from 'src/app/shared/shared-services/snack.service';
import { startWith, map } from 'rxjs/operators';
import { MatSelectChange } from '@angular/material';
import { PesquisaAreaCampus } from '../../models/pesquisa.area.model';
import { CustomValidators } from 'src/app/shared/shared-validators/custom-validators';

const quantidadeIndex = 5;
const TODOS = 'todos';

@Component({
  selector: 'app-projetos-area-conhecimento',
  templateUrl: './projetos-area-conhecimento.component.html',
  styleUrls: ['./projetos-area-conhecimento.component.css']
})
export class ProjetosAreaConhecimentoComponent implements OnInit {

  dataSet: PesquisaAreaCampus[];

  @Input('resultado') set resultadoFn(dataSet: PesquisaAreaCampus[]) {
    this.initChart(dataSet);
    this.dataSet = dataSet;
  }
  // tslint:disable-next-line: no-output-rename
  @Output('consultarPorCampus') consultarPorCampus: EventEmitter<any> = new EventEmitter();


  cores: string[] = [];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
      display: false
    },
    tooltips: {
      callbacks: {
        label: (item, data) => {
          return data.labels[item.index] + ': ' +
            data.datasets[0].data[item.index].toString().replace('.', ',') + ' %';
        }
      }
    }
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [];
  campisOptions: Campus[] = Campus.getTodos();

  form: FormGroup;
  filteredOptions: Observable<Campus[]>;
  campusSelecionado: string;

  constructor(
    private utilChartService: UtilChartService,
    private fb: FormBuilder,
    private snackService: SnackService) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      quantidadeIndex: [null],
      campusNome: [null,  CustomValidators.onlyNumberLetters],
    });

    this.filteredOptions = this.form.get('campusNome').valueChanges.pipe(
      startWith(''),
      map(value => this.filterCampus(value))
    );

  }

  private initChart(resultado) {
    this.calcularTotalPercentual(resultado, this.pieChartData, quantidadeIndex);
    this.configurarChart(resultado, quantidadeIndex);
  }


  private configurarChart(data: PesquisaAreaCampus[], quantidade: number) {
    for (let index = 0; index < quantidade; index++) {
      this.cores.push(this.utilChartService.dynamicColorsRGBA(0.4));
      this.pieChartLabels.push(data[index].areaConhecimento);
    }
    this.pieChartColors = [{
      backgroundColor: this.cores
    }];
  }

  private async calcularTotalPercentual(data: PesquisaAreaCampus[], pieChartData: number[], quantidadeIndex: number) {
    const totalProjetos = await this.getTotalProjetos(data);
    for (let index = 0; index < quantidadeIndex; index++) {
      const percentual = (data[index].total / totalProjetos) * 100.0;
      pieChartData.push(Number(percentual.toFixed(2)));
    }
  }

  async getTotalProjetos(data: PesquisaAreaCampus[]) {
    const somaTotal: number = data.reduce((soma, dado) => {
      return soma + dado.total;
    }, 0);
    return somaTotal;
  }

  private filterCampus(value: string): Campus[] {
    const filterValue = value ? value.toLowerCase() : '';
    return this.campisOptions.filter((option: Campus) => option.campus.toLowerCase().indexOf(filterValue) === 0);
  }

  atualizarChart(event: MatSelectChange) {
    this.resetChart();
    if (event.value !== TODOS) {
      let quantidade = Number(event.value);
      if (quantidade > this.dataSet.length) {
        quantidade = this.dataSet.length;
      }
      this.calcularTotalPercentual(this.dataSet, this.pieChartData, quantidade);
      this.configurarChart(this.dataSet, quantidade);
    } else {
      this.calcularTotalPercentual(this.dataSet, this.pieChartData, this.dataSet.length);
      this.configurarChart(this.dataSet, this.dataSet.length);
    }
  }

  private resetChart(): void {
    this.pieChartData = [];
    this.pieChartColors = [];
    this.pieChartLabels = [];
  }

  pesquisarPorCampus() {
    if (this.form.get('campusNome').valid && this.form.get('campusNome').value) {
      const nome = this.form.get('campusNome').value === 'Todos' ? undefined : this.form.get('campusNome').value; 
      this.resetChart();
      this.form.reset();
      this.campusSelecionado = nome;
      this.consultarPorCampus.emit(nome);
    } else {
      this.snackService.advertir('Para filtrar e necessário selecionar o campus');
    }
  }

  getCampo(nomeCampo: string) {
    return this.form.get(nomeCampo);
  }

}
