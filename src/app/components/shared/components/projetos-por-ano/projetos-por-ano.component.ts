import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { Campus } from 'src/app/shared/shared-models/models/campus.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { SnackService } from 'src/app/shared/shared-services/snack.service';
import { startWith, map } from 'rxjs/operators';
import { PesquisaAno } from '../../models/pesquisa.ano.model';
import { CustomValidators } from 'src/app/shared/shared-validators/custom-validators';

@Component({
  selector: 'app-projetos-por-ano',
  templateUrl: './projetos-por-ano.component.html',
  styleUrls: ['./projetos-por-ano.component.css']
})
export class ProjetosPorAnoComponent implements OnInit {

  dataSet: PesquisaAno[];
  // tslint:disable-next-line: no-input-rename
  @Input('titulo') titulo: string;

  @Input('resultado') set resultadoFn(dataSet: PesquisaAno[]) {
    this.initChart(dataSet);
    this.dataSet = dataSet;
  }

  // tslint:disable-next-line: no-output-rename
  @Output('consultarPorCampus') consultarPorCampus: EventEmitter<any> = new EventEmitter();

  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [],
      yAxes: []
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'red',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'red',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [];
  public lineChartLegend = false;
  public lineChartType = 'line';

  campisOptions: Campus[] = Campus.getTodos();
  form: FormGroup;
  filteredOptions: Observable<Campus[]>;
  campusSelecionado: string;

  constructor(
    private fb: FormBuilder,
    private snackService: SnackService) {
  }
  

  ngOnInit() {
    this.form = this.fb.group({
      quantidadeIndex: [null],
      campusNome: [null, CustomValidators.onlyNumberLetters],
    });

    this.filteredOptions = this.form.get('campusNome').valueChanges.pipe(
      startWith(''),
      map(value => this.filterCampus(value))
    );

  }

  getCampo(nomeCampo: string) {
    return this.form.get(nomeCampo);
  }

  private initChart(dataSet: PesquisaAno[]): void {
    this.setColor();
    dataSet.forEach((item: PesquisaAno) => {
      this.lineChartLabels.push(item.ano);
    });
    const dataMap = dataSet.map(d => { return d.total });
    this.lineChartData.push({ data: dataMap });
  }

  private filterCampus(value: string): Campus[] {
    const filterValue = value ? value.toLowerCase() : '';
    return this.campisOptions.filter((option: Campus) => option.campus.toLowerCase().indexOf(filterValue) === 0);
  }

  pesquisarPorCampus() {
    if (this.form.get('campusNome').valid && this.form.get('campusNome').value) {
      const nome = this.form.get('campusNome').value === 'Todos' ? undefined : this.form.get('campusNome').value;
      this.resetChart();
      this.form.reset();
      this.campusSelecionado = nome;
      this.consultarPorCampus.emit(nome);
    } else {
      this.snackService.advertir('Para filtrar e necessário selecionar o campus');
    }
  }

  private resetChart() {
    this.lineChartData = [];
    this.lineChartLabels = [];
  }

  private setColor() {
    this.lineChartColors = [
      {
        backgroundColor: 'rgba(169,232,174,0.3)',
        borderColor: 'rgb(30,134,35)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }
    ];
  }

}
