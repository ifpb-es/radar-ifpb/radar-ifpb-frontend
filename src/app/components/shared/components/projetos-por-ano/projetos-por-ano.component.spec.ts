import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetosPorAnoComponent } from './projetos-por-ano.component';

describe('ProjetosPorAnoComponent', () => {
  let component: ProjetosPorAnoComponent;
  let fixture: ComponentFixture<ProjetosPorAnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjetosPorAnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetosPorAnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
