import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumeroProjetoCampusComponent } from './numero-projeto-campus.component';

describe('NumeroProjetoCampusComponent', () => {
  let component: NumeroProjetoCampusComponent;
  let fixture: ComponentFixture<NumeroProjetoCampusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumeroProjetoCampusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumeroProjetoCampusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
