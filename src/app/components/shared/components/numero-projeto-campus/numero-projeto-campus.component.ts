import { Component, OnInit, Input } from '@angular/core';
import { ChartType, ChartDataSets, ChartOptions } from 'chart.js';
import { PesquisaCampus } from '../../models/pesquisa.campus.model';

@Component({
  selector: 'app-numero-projeto-campus',
  templateUrl: './numero-projeto-campus.component.html',
  styleUrls: ['./numero-projeto-campus.component.css']
})
export class NumeroProjetoCampusComponent implements OnInit {

  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: (item, data) => {
          return data.datasets[item.datasetIndex].label + ': ' + item.value ;
        }
      }
    }
  };
  public barChartLabels = [''];
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartData: ChartDataSets[] = [];

  // tslint:disable-next-line: no-input-rename
  @Input('resultado') numeroProjeto: PesquisaCampus[];
  // tslint:disable-next-line: no-input-rename
  @Input('titulo') titulo: string;

  constructor() {
  }

  ngOnInit() {
    this.numeroProjeto.forEach(i => {
      this.barChartData.push({ data: [i.total], label: i.campus });
    });
  }


}
