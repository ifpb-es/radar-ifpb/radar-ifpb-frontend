import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilChartService } from './util.chart.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ],
    providers: [
        UtilChartService
    ]
})
export class SharedServicesHomeModule { }
