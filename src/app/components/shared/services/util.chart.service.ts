import { Injectable } from '@angular/core';
/**
 * Todas as funções desse serviço são possíveis candidatos a se tornarem Pipes futuramante
 */
@Injectable()
export class UtilChartService {

    public dynamicColorsRGBA(alfa: number): string {
        const r = Math.floor(Math.random() * 255);
        const g = Math.floor(Math.random() * 255);
        const b = Math.floor(Math.random() * 255);
        return 'rgba(' + r + ',' + g + ',' + b + ',' + alfa + ')';
    }


}
