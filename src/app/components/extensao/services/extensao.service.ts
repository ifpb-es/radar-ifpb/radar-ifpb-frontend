import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/shared/shared-services/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PesquisaCampus } from '../../shared/models/pesquisa.campus.model';
import { PesquisaAreaCampus } from '../../shared/models/pesquisa.area.model';
import { PesquisaAno } from '../../shared/models/pesquisa.ano.model';
import { ExtensaoBolsa } from '../../shared/models/extensao.bolsa.model';

const PATHS = {
  pesquisas: 'projetos/extensao',
  campus: 'campus',
  areaConhecimento: 'areas/conhecimentos',
  ano: 'ano',
  bolsas: 'bolsas'
};

@Injectable()
export class ExtensaoService extends BaseService {

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  consultarTotalProjetoPorCampus(): Observable<PesquisaCampus[]> {
    return this.httpClient.get<PesquisaCampus[]>(`${this.urlBase}/${PATHS.pesquisas}/${PATHS.campus}`);
  }

  consultarTotalProjetoPorAreaConhecimento(nomeCampus?: string): Observable<PesquisaAreaCampus[]> {
    return this.httpClient.get<PesquisaAreaCampus[]>(`${this.urlBase}/${PATHS.pesquisas}/${PATHS.areaConhecimento}`, 
    { params: this.setParam(nomeCampus) });
  }

  consultarTotalProjetoPorAno(nomeCampus?: string): Observable<PesquisaAno[]> {
    return this.httpClient.get<PesquisaAno[]>(`${this.urlBase}/${PATHS.pesquisas}/${PATHS.ano}`, { params: this.setParam(nomeCampus) });
  }

  consultarValoresInvestidoPorAno(nomeCampus?: string): Observable<ExtensaoBolsa[]> {
    return this.httpClient.get<ExtensaoBolsa[]>(`${this.urlBase}/${PATHS.pesquisas}/${PATHS.bolsas}`,
      { params: this.setParam(nomeCampus) });
  }

  private setParam(nomeCampus?: string): HttpParams {
    let param = new HttpParams();
    if (nomeCampus) {
      param = param.append('campus', nomeCampus);
    }
    return param;
  }

}
