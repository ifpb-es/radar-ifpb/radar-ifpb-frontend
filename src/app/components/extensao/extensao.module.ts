import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExtensaoComponent } from './components/extensao.component';
import { ExtensaoRoutingModule } from './extensao-routing.module';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { SharedComponentsModule } from 'src/app/shared/shared-components/shared-components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedComponentsHomeModule } from '../shared/components/shared.component.module';
import { SharedServicesHomeModule } from '../shared/services/shared.component.module';
import { ExtensaoService } from './services/extensao.service';
import { ValoresBolsaPorAnoComponent } from './components/valores-bolsa-por-ano/valores-bolsa-por-ano.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [ExtensaoComponent, ValoresBolsaPorAnoComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    AngularMaterialModule,
    SharedComponentsModule,
    SharedComponentsHomeModule,
    SharedServicesHomeModule,
    ExtensaoRoutingModule,
    ChartsModule
  ],
  providers: [
    ExtensaoService
  ]
})
export class ExtensaoModule { }
