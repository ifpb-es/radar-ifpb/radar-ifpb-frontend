import { Component, OnInit } from '@angular/core';
import { ExtensaoService } from '../services/extensao.service';
import { PesquisaCampus } from '../../shared/models/pesquisa.campus.model';
import { PesquisaAreaCampus } from '../../shared/models/pesquisa.area.model';
import { PesquisaAno } from '../../shared/models/pesquisa.ano.model';
import { UtilidadesService } from 'src/app/core/services/utilidades.service';
import { forkJoin } from 'rxjs';
import { ExtensaoBolsa } from '../../shared/models/extensao.bolsa.model';
import { SnackService } from 'src/app/shared/shared-services/snack.service';

@Component({
  selector: 'app-extensao',
  templateUrl: './extensao.component.html',
  styleUrls: ['./extensao.component.css']
})
export class ExtensaoComponent implements OnInit {

  numeroProjetoPorCampus: PesquisaCampus[];
  projetosPorAreaConhecimento: PesquisaAreaCampus[];
  projetosPorAno: PesquisaAno[];
  valoresInvestidoBolsa: ExtensaoBolsa[];

  constructor(
    private extensaoService: ExtensaoService,
    private utilizadeService: UtilidadesService,
    private snackService: SnackService
  ) { }

  ngOnInit() {
    forkJoin(
      this.extensaoService.consultarTotalProjetoPorCampus(),
      this.extensaoService.consultarTotalProjetoPorAreaConhecimento(),
      this.extensaoService.consultarTotalProjetoPorAno(),
      this.extensaoService.consultarValoresInvestidoPorAno()
    ).subscribe(resultado => {

      this.setNumeroProjetoPorCampus(resultado[0]);
      this.setProjetosPorAreaConhecimento(resultado[1]);
      this.setProjetosPorAno(resultado[2]);
      this.setValoresInvestidos(resultado[3]);
    }, () => {
      this.snackService.alertar('Alguma coisa não saiu como esperado, por favor tente novamente mais tarde!');
    });
  }

  consultarNumeroProjetoPorArea(campus?: string) {
    this.extensaoService.consultarTotalProjetoPorAreaConhecimento(campus).subscribe((resultado: PesquisaAreaCampus[]) => {
      if (Array.isArray(resultado)) {
        this.setProjetosPorAreaConhecimento(resultado);
      } else {
        this.tratarRetorno();
      }
    }, error => {
      console.error(error);
      this.tratarErro();
    });
  }

  async setProjetosPorAreaConhecimento(resultado: PesquisaAreaCampus[]) {
    resultado = await this.utilizadeService.compareToList(resultado, 'total');
    resultado.reverse();
    this.projetosPorAreaConhecimento = resultado;
  }

  consultarProjetoPorAno(campus?: string) {
    this.extensaoService.consultarTotalProjetoPorAno(campus).subscribe((resultado: PesquisaAno[]) => {
      this.setProjetosPorAno(resultado);
      if (Array.isArray(resultado)) {
        this.setValoresInvestidos(resultado);
      } else {
        this.tratarRetorno();
      }
    }, error => {
      console.error(error);
      this.tratarErro();
    });
  }

  async setProjetosPorAno(resultado: PesquisaAno[]) {
    resultado = await this.utilizadeService.compareToList(resultado, 'ano');
    this.projetosPorAno = resultado;
  }

  consultarValoresInvestimento(campus?: string) {
    this.extensaoService.consultarValoresInvestidoPorAno(campus).subscribe((resultado: ExtensaoBolsa[]) => {
      if (Array.isArray(resultado)) {
        this.setValoresInvestidos(resultado);
      } else {
        this.tratarRetorno();
      }
    }, error => {
      console.error(error);
      this.tratarErro();
    });
  }

  async setValoresInvestidos(resultado: ExtensaoBolsa[]) {
    resultado = await this.utilizadeService.compareToList(resultado, 'ano');
    this.valoresInvestidoBolsa = resultado;
  }

  private setNumeroProjetoPorCampus(resultado: PesquisaCampus[]) {
    if (Array.isArray(resultado)) {
      this.numeroProjetoPorCampus = resultado;
    } else {
      this.tratarRetorno();
    }
  }

  private tratarRetorno(): void {
    this.snackService.informar('Nenhum resultado encontrado.');
  }

  private tratarErro(): void {
    this.snackService.alertar('Não foi possível realizar ação. Tente novamente mais tarde.');
  }
}
