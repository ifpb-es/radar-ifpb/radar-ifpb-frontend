import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValoresBolsaPorAnoComponent } from './valores-bolsa-por-ano.component';

describe('ValoresBolsaPorAnoComponent', () => {
  let component: ValoresBolsaPorAnoComponent;
  let fixture: ComponentFixture<ValoresBolsaPorAnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValoresBolsaPorAnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValoresBolsaPorAnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
