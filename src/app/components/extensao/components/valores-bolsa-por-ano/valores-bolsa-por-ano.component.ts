import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ExtensaoBolsa } from 'src/app/components/shared/models/extensao.bolsa.model';
import { Label } from 'ng2-charts';
import { ChartType, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-valores-bolsa-por-ano',
  templateUrl: './valores-bolsa-por-ano.component.html',
  styleUrls: ['./valores-bolsa-por-ano.component.css']
})
export class ValoresBolsaPorAnoComponent implements OnInit {
  dataSet: ExtensaoBolsa[];

  @Input('resultado') set resultadoFn(dataSet: ExtensaoBolsa[]) {
    this.initChart(dataSet);
    this.dataSet = dataSet;
  }

  // tslint:disable-next-line: no-output-rename
  @Output('consultarPorCampus') consultarPorCampus: EventEmitter<any> = new EventEmitter();

  public doughnutChartLabels: Label[] = [];
  public doughnutChartData: number[] = [];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top'
    },
    tooltips: {
      callbacks: {
        label: (item, data) => {
          const formatter = new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
          });
          return data.labels[item.index] + ':  ' +
          formatter.format(Number(data.datasets[0].data[item.index]));
        }
      }
    }
  };
  constructor() { }

  ngOnInit() {
  }

  private initChart(dataSet: ExtensaoBolsa[]): void {
    dataSet.forEach((item: ExtensaoBolsa) => {
      this.doughnutChartLabels.push(item.ano);
      this.doughnutChartData.push(item.total);
    });
  }

}
